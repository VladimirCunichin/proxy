# syntax=docker/dockerfile:1

FROM golang:1.17-alpine as builder
RUN apk update && apk add --no-cache git

WORKDIR /proxy

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY .  .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o bin/docker-proxy cmd/main.go


FROM scratch

COPY --from=builder /proxy/bin .
COPY .  .

EXPOSE 8080

CMD ["/docker-proxy"]