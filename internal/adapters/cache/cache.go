package cache

import (
	"container/list"
	"sync"

	"bitbucket.org/VladimirCunichin/proxy/internal/domain/entities"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/errors"
)

type KeyPair struct {
	key   string
	value *entities.Response
}

type LRUCache struct {
	mu       sync.Mutex
	capacity int
	list     list.List
	elements map[string]*list.Element
}

func NewLRU(cap int) LRUCache {
	return LRUCache{
		elements: map[string]*list.Element{},
		capacity: cap,
	}
}

func (cache *LRUCache) GetCapacity() int {
	return cache.capacity
}

func (cache *LRUCache) Get(key string) (*entities.Response, error) {
	cache.mu.Lock()
	defer cache.mu.Unlock()
	if node, ok := cache.elements[key]; ok {
		value := node.Value.(*list.Element).Value.(KeyPair).value
		cache.list.MoveToFront(node)
		return value, nil
	}
	return nil, errors.NotFound

}

func (cache *LRUCache) Set(key string, value *entities.Response) {
	cache.mu.Lock()
	defer cache.mu.Unlock()
	if cache.capacity == 0 {
		return
	}
	if node, ok := cache.elements[key]; ok {
		cache.list.MoveToFront(node)
		node.Value.(*list.Element).Value = KeyPair{key: key, value: value}
	} else {
		if cache.list.Len() == cache.capacity {
			idx := cache.list.Back().Value.(*list.Element).Value.(KeyPair).key
			delete(cache.elements, idx)
			cache.list.Remove(cache.list.Back())
		}
	}
	node := &list.Element{
		Value: KeyPair{
			key:   key,
			value: value,
		},
	}
	pointer := cache.list.PushFront(node)
	cache.elements[key] = pointer
}

func (cache *LRUCache) Remove(key string) {
	cache.mu.Lock()
	defer cache.mu.Unlock()
	if node, ok := cache.elements[key]; ok {
		delete(cache.elements, key)
		cache.list.Remove(node)
	}
}
