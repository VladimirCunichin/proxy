package inmemory

import (
	"context"
	"reflect"
	"testing"

	"bitbucket.org/VladimirCunichin/proxy/internal/domain/entities"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/errors"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	storage1 := NewStorage()
	storage2 := NewStorage()
	if !reflect.DeepEqual(storage1, storage2) {
		t.Errorf("Not equal data in storage: %v, %v", storage1, storage2)
	}
}

var id1 uuid.UUID
var id2 uuid.UUID

func prepareStorage() *Storage {
	storage := NewStorage()
	id1 = uuid.NewV4()
	id2 = uuid.NewV4()
	storage.data = map[uuid.UUID]entities.SaveData{
		id1: {
			Id: id1,
			Request: &entities.Request{
				Method: "GET",
				Url:    "http://google.com",
				RequestHeaders: map[string][]string{
					"Authentication": {"Basic	bG9naW46cGFzc3dvcmQ="},
					"Content-Type": {"application/json"},
				},
				Body: entities.Body{},
			},
			Response: &entities.SaveResponse{
				Status: 200,
				Headers: map[string][]string{
					"Content-Type": {"application/json"},
				},
				Length: 69,
			},
		},
		id2: {
			Id: id2,
			Request: &entities.Request{
				Method: "GET",
				Url:    "http://google.com",
				RequestHeaders: map[string][]string{
					"Authentication": {"Basic	bG9naW46cGFzc3dvcmQ="},
					"Content-Type": {"application/json"},
				},
				Body: entities.Body{},
			},
			Response: &entities.SaveResponse{
				Status: 200,
				Headers: map[string][]string{
					"Content-Type": {"application/json"},
				},
				Length: 420,
			},
		},
	}
	return storage
}

func TestStorage_Add(t *testing.T) {
	storage := NewStorage()

	newData := entities.SaveData{
		Id: uuid.NewV4(),
		Request: &entities.Request{
			Method: "GET",
			Url:    "http://google.com",
			RequestHeaders: map[string][]string{
				"Authentication": {"Basic	bG9naW46cGFzc3dvcmQ="},
				"Content-Type": {"application/json"},
			},
			Body: entities.Body{},
		},
		Response: &entities.SaveResponse{
			Status: 200,
			Headers: map[string][]string{
				"Content-Type": {"application/json"},
			},
			Length: 69,
		},
	}
	err := storage.SaveData(context.Background(), newData)
	assert.Nil(t, err)
	assert.Equal(t, len(storage.data), 1, "storage len should be 1")
}

func TestAddToNotEmpty(t *testing.T) {
	storage := prepareStorage()
	newData := entities.SaveData{
		Id: uuid.NewV4(),
		Request: &entities.Request{
			Method: "GET",
			Url:    "http://google.com",
			RequestHeaders: map[string][]string{
				"Authentication": {"Basic	bG9naW46cGFzc3dvcmQ="},
				"Content-Type": {"application/json"},
			},
			Body: entities.Body{},
		},
		Response: &entities.SaveResponse{
			Status: 200,
			Headers: map[string][]string{
				"Content-Type": {"application/json"},
			},
			Length: 123,
		},
	}
	err := storage.SaveData(context.Background(), newData)
	assert.Nil(t, err)
	assert.Len(t, storage.data, 3)
}

func TestAddToExistingId(t *testing.T) {
	storage := prepareStorage()
	newData := entities.SaveData{
		Id: id1,
		Request: &entities.Request{
			Method: "GET",
			Url:    "http://google.com",
			RequestHeaders: map[string][]string{
				"Authentication": {"Basic	bG9naW46cGFzc3dvcmQ="},
				"Content-Type": {"application/json"},
			},
			Body: entities.Body{},
		},
		Response: &entities.SaveResponse{
			Status: 200,
			Headers: map[string][]string{
				"Content-Type": {"application/json"},
			},
			Length: 69,
		},
	}
	err := storage.SaveData(context.Background(), newData)
	assert.NotNil(t, err)
}

func TestDelete(t *testing.T) {
	storage := prepareStorage()
	err := storage.Delete(context.Background(), id1)
	assert.Nil(t, err)
	assert.Len(t, storage.data, 1)
}

func TestDeleteWrong(t *testing.T) {
	storage := prepareStorage()
	err := storage.Delete(context.Background(), uuid.NewV4())
	assert.Equal(t, err, errors.NotFound, " error should be NotFound")
}

func TestGetData(t *testing.T) {
	storage := prepareStorage()
	data, err := storage.GetData(context.Background())
	assert.Nil(t, err)
	assert.Len(t, data, len(storage.data))
	for _, e := range data {
		assert.Equal(t, e, storage.data[e.Id], "GetData returned wrong data")
	}
}

func TestGetDataEmpty(t *testing.T) {
	storage := NewStorage()
	_, err := storage.GetData(context.Background())
	assert.NotNil(t, err)
}

func TestGetDataById(t *testing.T) {
	storage := prepareStorage()
	data, err := storage.GetDataByID(context.Background(), id1)
	assert.Nil(t, err)
	assert.Equal(t, data, storage.data[data.Id], "data is not equal")
}
