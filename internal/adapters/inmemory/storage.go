package inmemory

import (
	"context"
	"sync"

	"bitbucket.org/VladimirCunichin/proxy/internal/domain/entities"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/errors"
	uuid "github.com/satori/go.uuid"
)

type Storage struct {
	mu   sync.Mutex
	data map[uuid.UUID]entities.SaveData
}

func NewStorage() *Storage {
	return &Storage{data: make(map[uuid.UUID]entities.SaveData)}
}

func (storage *Storage) SaveData(ctx context.Context, data entities.SaveData) error {
	_, ok := storage.data[data.Id]
	if ok {
		return errors.DataAlreadyExists
	}
	storage.mu.Lock()
	storage.data[data.Id] = data
	storage.mu.Unlock()
	return nil
}

func (storage *Storage) GetDataByID(ctx context.Context, id uuid.UUID) (entities.SaveData, error) {
	storage.mu.Lock()
	e, ok := storage.data[id]
	storage.mu.Unlock()
	if !ok {
		return entities.SaveData{}, errors.NotFound
	}
	return e, nil
}

func (storage *Storage) GetData(ctx context.Context) ([]entities.SaveData, error) {
	if len(storage.data) > 0 {
		data := make([]entities.SaveData, 0, len(storage.data))
		storage.mu.Lock()
		for _, e := range storage.data {
			data = append(data, e)
		}
		storage.mu.Unlock()
		if len(data) > 0 {
			return data, nil
		}
	}
	return []entities.SaveData{}, errors.NotFound
}

func (storage *Storage) Delete(ctx context.Context, id uuid.UUID) error {
	storage.mu.Lock()
	defer storage.mu.Unlock()
	_, ok := storage.data[id]
	if !ok {
		return errors.NotFound
	}
	delete(storage.data, id)
	return nil
}

func (storage *Storage) Length() int {
	return len(storage.data)
}
