package sql

import (
	"context"
	"testing"
	"time"

	"bitbucket.org/VladimirCunichin/proxy/internal/domain/config"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/entities"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

func TestSQL(t *testing.T) {
	dbconf := config.Config{
		DBUser:     "postgres",
		DBPassword: "Temp12xd34",
		DBDatabase: "proxy",
		DBHost:     "localhost",
		DBPort:     "5432",
	}
	var storage *Storage = NewStorage(&dbconf)

	var id uuid.UUID = uuid.NewV4()
	var testData entities.SaveData = entities.SaveData{
		Id: id,
		Request: &entities.Request{
			Method: "GET",
			Url:    "http://google.com",
			RequestHeaders: map[string][]string{
				"Authentication": {"Basic	bG9naW46cGFzc3dvcmQ="},
				"Content-Type": {"application/json"},
			},
			Body: entities.Body{},
		},
		Response: &entities.SaveResponse{
			Status: 200,
			Headers: map[string][]string{
				"Content-Type": {"application/json"},
			},
			Length: 69,
		},
	}

	ctx, _ := context.WithTimeout(context.Background(), time.Second)
	err := storage.SaveData(ctx, testData)
	assert.Nil(t, err)

	ctx, _ = context.WithTimeout(context.Background(), time.Second)
	getData, err := storage.GetData(ctx)
	assert.Nil(t, err)
	for _, v := range getData {
		if v.Id == id {
			assert.Equal(t, testData, v)
		}
	}
	ctx, _ = context.WithTimeout(context.Background(), time.Second)
	data1, err := storage.GetDataByID(ctx, id)
	assert.Nil(t, err)
	assert.Equal(t, testData, data1, "data should be equal")
	ctx, _ = context.WithTimeout(context.Background(), time.Second)
	err = storage.Delete(ctx, testData.Id)
	assert.Nil(t, err)
	ctx, _ = context.WithTimeout(context.Background(), time.Second)
	deletedData, err := storage.GetDataByID(ctx, testData.Id)
	assert.Nil(t, err)
	assert.Equal(t, entities.SaveData{}, deletedData, "data should be equal")
}
