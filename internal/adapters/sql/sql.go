package sql

import (
	"context"
	"fmt"

	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"bitbucket.org/VladimirCunichin/proxy/internal/domain/config"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/entities"
	"bitbucket.org/VladimirCunichin/proxy/internal/logger"
)

type Storage struct {
	db  *gorm.DB
	ctx context.Context
}

func NewStorage(dbconf *config.Config) *Storage {
	return &Storage{
		db:  InitDB(dbconf),
		ctx: context.Background(),
	}
}

func InitDB(conf *config.Config) *gorm.DB {
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable", conf.DBHost, conf.DBUser, conf.DBPassword, conf.DBDatabase, conf.DBPort)
	newdb, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		logger.Error("err while opening gorm", "err", err)
	}
	newdb.AutoMigrate(&entities.SaveData{})
	return newdb
}

func (storage *Storage) SaveData(ctx context.Context, data entities.SaveData) error {
	if err := storage.db.WithContext(ctx).Create(&data).Error; err != nil {
		return errors.Wrap(err, "error while adding to db")
	}
	return nil
}

func (storage *Storage) GetDataByID(ctx context.Context, id uuid.UUID) (entities.SaveData, error) {
	var data entities.SaveData
	if err := storage.db.WithContext(ctx).Where("id = ?", id).Find(&data).Error; err != nil {
		return data, errors.Wrap(err, "error while getting daba by id from db")
	}
	return data, nil
}

func (storage *Storage) GetData(ctx context.Context) ([]entities.SaveData, error) {
	var getData []entities.SaveData
	if err := storage.db.WithContext(ctx).Find(&getData).Error; err != nil {
		return getData, errors.Wrap(err, "error while getting data from db")
	}
	return getData, nil
}

func (storage *Storage) Delete(ctx context.Context, id uuid.UUID) error {
	if err := storage.db.WithContext(ctx).Delete(&entities.SaveData{Id: id}).Error; err != nil {
		return errors.Wrap(err, "error while deleting from db")
	}
	return nil
}
