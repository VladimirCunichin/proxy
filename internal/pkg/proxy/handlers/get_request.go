package handlers

import (
	"context"
	"strings"

	"bitbucket.org/VladimirCunichin/proxy/internal/adapters/cache"
	"bitbucket.org/VladimirCunichin/proxy/internal/adapters/inmemory"
	"bitbucket.org/VladimirCunichin/proxy/internal/adapters/sql"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/config"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/errors"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/usecases"
	"bitbucket.org/VladimirCunichin/proxy/internal/logger"
	"bitbucket.org/VladimirCunichin/proxy/internal/pkg/proxy/models"
	"bitbucket.org/VladimirCunichin/proxy/internal/pkg/proxy/restapi/operations/proxy"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"
	uuid "github.com/satori/go.uuid"
)

type GetRequestImpl struct {
	cache          cache.LRUCache
	requestStorage *usecases.RequestStorage
	ctx            context.Context
}

func NewGetRequestHandler(conf *config.Config) proxy.GetRequestHandler {
	return &GetRequestImpl{
		cache: cache.NewLRU(conf.CacheSize),
		requestStorage: func() *usecases.RequestStorage {
			if strings.EqualFold(conf.Storage, "sql") {
				return usecases.New(sql.NewStorage(conf))
			}
			return usecases.New(inmemory.NewStorage())
		}(), ctx: context.Background(),
	}
}

func (impl *GetRequestImpl) Handle(params proxy.GetRequestParams) middleware.Responder {
	id, err := uuid.FromString(params.ID.String())
	if err != nil {
		logger.Error("failed to convert id to uuid", "error", err)
		return proxy.NewGetRequestBadRequest()
	}
	data, err := impl.requestStorage.GetDataByID(context.Background(), id)
	if err != nil {
		if err == errors.NotFound {
			return proxy.NewGetRequestNotFound()
		}
		logger.Error("error while retrieving data from storage by id", "error", err)
	}
	responseVal := &models.SaveData{
		ID: strfmt.UUID(data.Id.String()),
		Response: &models.SaveResponse{
			Headers: models.Headers(data.Response.Headers),
			Length:  data.Response.Length,
			Status:  int64(data.Response.Status),
		},
		Request: &models.Request{
			Method:  &data.Request.Method,
			URL:     &data.Request.Url,
			Headers: models.Headers(data.Request.RequestHeaders),
			Body:    models.Body(data.Request.Body),
		},
	}
	return proxy.NewGetRequestOK().WithPayload(responseVal)
}
