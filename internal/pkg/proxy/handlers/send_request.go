package handlers

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"bitbucket.org/VladimirCunichin/proxy/internal/adapters/cache"
	"bitbucket.org/VladimirCunichin/proxy/internal/adapters/inmemory"
	"bitbucket.org/VladimirCunichin/proxy/internal/adapters/sql"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/config"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/entities"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/usecases"
	myhttp "bitbucket.org/VladimirCunichin/proxy/internal/http"
	"bitbucket.org/VladimirCunichin/proxy/internal/logger"
	"bitbucket.org/VladimirCunichin/proxy/internal/pkg/proxy/models"
	"bitbucket.org/VladimirCunichin/proxy/internal/pkg/proxy/restapi/operations/proxy"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"
	uuid "github.com/satori/go.uuid"
)

type SendRequestImpl struct {
	cache          cache.LRUCache
	requestStorage *usecases.RequestStorage
	ctx            context.Context
}

func NewSendRequestHandler(conf *config.Config) proxy.SendRequestHandler {
	return &SendRequestImpl{
		cache: cache.NewLRU(conf.CacheSize),
		requestStorage: func() *usecases.RequestStorage {
			if strings.EqualFold(conf.Storage, "sql") {
				return usecases.New(sql.NewStorage(conf))
			}
			return usecases.New(inmemory.NewStorage())
		}(), ctx: context.Background(),
	}
}

func (impl *SendRequestImpl) Handle(params proxy.SendRequestParams) middleware.Responder {
	inputRequest := params.Input
	req := entities.Request{
		Url:            *inputRequest.URL,
		Method:         *inputRequest.Method,
		RequestHeaders: entities.Headers(inputRequest.Headers),
		Body:           entities.Body(inputRequest.Body),
	}
	client := myhttp.CustomClient()

	cacheReq, err := json.Marshal(req)
	if err != nil {
		logger.Error("error marshalling req", "err", err)
	}
	cacheKey := myhttp.SHA1(string(cacheReq))
	if cacheResp, err := impl.cache.Get(cacheKey); err == nil {
		resp := &models.Response{
			Headers: models.Headers(cacheResp.Headers),
			ID:      strfmt.UUID(cacheResp.Id.String()),
			Length:  cacheResp.Length,
			Status:  int64(cacheResp.Status),
		}
		return proxy.NewSendRequestOK().WithPayload(resp)
	}
	switch req.Method {
	case "GET":
		request, err := http.NewRequest("GET", req.Url, nil)
		if err != nil {
			logger.Error("error while creating a http request", "error", err)
			return proxy.NewSendRequestInternalServerError()
		}
		for k, v := range req.RequestHeaders {
			for _, header := range v {
				request.Header.Add(k, header)
			}
		}
		resp, err := client.Do(request)
		if err != nil {
			logger.Error("error while doing the request", "err", err)
			return proxy.NewSendRequestInternalServerError()
		}
		if resp.ContentLength == -1 {
			b, _ := ioutil.ReadAll(resp.Body)
			resp.ContentLength = int64(len(b))
		}
		defer resp.Body.Close()
		response := entities.Response{
			Id:      uuid.NewV4(),
			Status:  resp.StatusCode,
			Headers: map[string][]string(resp.Header),
			Length:  resp.ContentLength,
		}
		err = impl.requestStorage.SaveData(context.Background(), &req, &response)
		if err != nil {
			logger.Error("error during save data", "error", err)
			return proxy.NewSendRequestInternalServerError()
		}
		impl.cache.Set(cacheKey, &response)
		swagResponse := &models.Response{
			Headers: models.Headers(response.Headers),
			ID:      strfmt.UUID(response.Id.String()),
			Length:  response.Length,
			Status:  int64(response.Status),
		}
		return proxy.NewSendRequestOK().WithPayload(swagResponse)
	case "POST":
		resp, err := myhttp.Post(req.Url, req.Body, req.RequestHeaders)
		if err != nil {
			logger.Error("error while posting", "err", err)
			return proxy.NewSendRequestInternalServerError()
		}
		if resp.ContentLength == -1 {
			b, _ := ioutil.ReadAll(resp.Body)
			resp.ContentLength = int64(len(b))
		}
		defer resp.Body.Close()
		response := entities.Response{
			Id:      uuid.NewV4(),
			Status:  resp.StatusCode,
			Headers: map[string][]string(resp.Header),
			Length:  resp.ContentLength,
		}
		err = impl.requestStorage.SaveData(context.Background(), &req, &response)
		impl.cache.Set(cacheKey, &response)
		if err != nil {
			logger.Error("error during save data", "error", err)
			return proxy.NewSendRequestInternalServerError()
		}
		swagResponse := &models.Response{
			ID:      strfmt.UUID(response.Id.String()),
			Headers: models.Headers(response.Headers),
			Length:  response.Length,
			Status:  int64(response.Status),
		}
		return proxy.NewSendRequestOK().WithPayload(swagResponse)
	default:
		logger.Error("Unexpected command", "command", req.Method)
		return proxy.NewSendRequestBadRequest()
	}
}
