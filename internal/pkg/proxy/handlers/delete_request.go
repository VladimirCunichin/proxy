package handlers

import (
	"context"
	"strings"

	"bitbucket.org/VladimirCunichin/proxy/internal/adapters/cache"
	"bitbucket.org/VladimirCunichin/proxy/internal/adapters/inmemory"
	"bitbucket.org/VladimirCunichin/proxy/internal/adapters/sql"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/config"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/errors"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/usecases"
	"bitbucket.org/VladimirCunichin/proxy/internal/logger"
	"bitbucket.org/VladimirCunichin/proxy/internal/pkg/proxy/restapi/operations/proxy"
	"github.com/go-openapi/runtime/middleware"
	uuid "github.com/satori/go.uuid"
)

type DeleteRequestImpl struct {
	cache          cache.LRUCache
	requestStorage *usecases.RequestStorage
	ctx            context.Context
}

func NewDeleteRequestHandler(conf *config.Config) proxy.DeleteRequestHandler {
	return &DeleteRequestImpl{
		cache: cache.NewLRU(conf.CacheSize),
		requestStorage: func() *usecases.RequestStorage {
			if strings.EqualFold(conf.Storage, "sql") {
				return usecases.New(sql.NewStorage(conf))
			}
			return usecases.New(inmemory.NewStorage())
		}(), ctx: context.Background(),
	}
}

func (impl *DeleteRequestImpl) Handle(params proxy.DeleteRequestParams) middleware.Responder {
	id, err := uuid.FromString(params.ID.String())
	if err != nil {
		logger.Error("failed to convert id to uuid", "error", err)
		return proxy.NewGetRequestBadRequest()
	}
	err = impl.requestStorage.Delete(context.Background(), id)
	if err != nil {
		if err == errors.NotFound {
			return proxy.NewGetRequestNotFound()
		}
		logger.Error("error while retrieving data from storage by id", "error", err)
	}
	return proxy.NewDeleteRequestOK().WithPayload("deleted successfully")
}
