// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"crypto/tls"
	"net/http"
	"os"
	"strconv"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"

	"bitbucket.org/VladimirCunichin/proxy/internal/domain/config"
	"bitbucket.org/VladimirCunichin/proxy/internal/logger"
	"bitbucket.org/VladimirCunichin/proxy/internal/pkg/proxy/handlers"
	"bitbucket.org/VladimirCunichin/proxy/internal/pkg/proxy/restapi/operations"
	"bitbucket.org/VladimirCunichin/proxy/internal/pkg/proxy/restapi/operations/proxy"
)

//go:generate swagger generate server --target ../../third_party --name ProxyApplication --spec ../../api/swagger.yml --principal interface{} --default-scheme proxy

func configureFlags(api *operations.ProxyApplicationAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.ProxyApplicationAPI) http.Handler {
	// configure the api here
	cacheSize, _ := strconv.Atoi(os.Getenv("CACHE_SIZE"))
	httpPort, _ := strconv.Atoi(os.Getenv("HTTP_PORT"))
	config := &config.Config{
		LogFile:    os.Getenv("LOG_FILE"),
		LogLevel:   os.Getenv("LOG_LEVEL"),
		CacheSize:  cacheSize,
		HttpIp:     os.Getenv("HTTP_IP"),
		HttpPort:   httpPort,
		DBUser:     os.Getenv("DB_USER"),
		DBPassword: os.Getenv("DB_PASSWORD"),
		DBHost:     os.Getenv("DB_HOST"),
		DBPort:     os.Getenv("DB_PORT"),
		DBDatabase: os.Getenv("DB_DATABASE"),
		Storage:    os.Getenv("STORAGE_TYPE"),
	}
	api.ServeError = errors.ServeError

	api.Logger = logger.Info

	api.UseSwaggerUI()
	// To continue using redoc as your UI, uncomment the following line
	// api.UseRedoc()

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	api.ProxyGetRequestHandler = handlers.NewGetRequestHandler(config)
	api.ProxySendRequestHandler = handlers.NewSendRequestHandler(config)
	api.ProxyDeleteRequestHandler = handlers.NewDeleteRequestHandler(config)

	if api.ProxyDeleteRequestHandler == nil {
		api.ProxyDeleteRequestHandler = proxy.DeleteRequestHandlerFunc(func(params proxy.DeleteRequestParams) middleware.Responder {
			return middleware.NotImplemented("operation proxy.DeleteRequest has not yet been implemented")
		})
	}
	if api.ProxyGetRequestHandler == nil {
		api.ProxyGetRequestHandler = proxy.GetRequestHandlerFunc(func(params proxy.GetRequestParams) middleware.Responder {
			return middleware.NotImplemented("operation proxy.GetRequest has not yet been implemented")
		})
	}
	if api.ProxySendRequestHandler == nil {
		api.ProxySendRequestHandler = proxy.SendRequestHandlerFunc(func(params proxy.SendRequestParams) middleware.Responder {
			return middleware.NotImplemented("operation proxy.SendRequest has not yet been implemented")
		})
	}

	api.PreServerShutdown = func() {}

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix".
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation.
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics.
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}
