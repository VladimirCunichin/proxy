package http

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"bitbucket.org/VladimirCunichin/proxy/internal/adapters/cache"
	"bitbucket.org/VladimirCunichin/proxy/internal/adapters/inmemory"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/entities"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/usecases"
	"github.com/stretchr/testify/assert"
)

func BenchmarkAdd(b *testing.B) {
	server = &Server{
		cache:          cache.NewLRU(5),
		requestStorage: usecases.New(inmemory.NewStorage()),
		ctx:            context.Background(),
	}
	for i := 0; i < b.N; i++ {
		body := strings.NewReader(`{"method":"GET","url":"https://jsonplaceholder.typicode.com/posts","headers":{"Authentication":["Basic\tbG9naW46cGFzc3dvcmQ="],"Content-Type":["application/json"]}}`)
		req := httptest.NewRequest(http.MethodGet, "/", body)
		w := httptest.NewRecorder()
		server.add(w, req)
		res := w.Result()
		data, err := ioutil.ReadAll(res.Body)
		assert.Nil(b, err)
		var response entities.Response
		err = json.Unmarshal(data, &response)
		assert.Nil(b, err)
	}
}

func BenchmarkAddWithoutCache(b *testing.B) {
	server = &Server{
		cache:          cache.NewLRU(0),
		requestStorage: usecases.New(inmemory.NewStorage()),
		ctx:            context.Background(),
	}
	for i := 0; i < b.N; i++ {
		body := strings.NewReader(`{"method":"GET","url":"https://jsonplaceholder.typicode.com/posts","headers":{"Authentication":["Basic\tbG9naW46cGFzc3dvcmQ="],"Content-Type":["application/json"]}}`)
		req := httptest.NewRequest(http.MethodGet, "/", body)
		w := httptest.NewRecorder()
		server.add(w, req)
		res := w.Result()
		data, err := ioutil.ReadAll(res.Body)
		assert.Nil(b, err)
		var response entities.Response
		err = json.Unmarshal(data, &response)
		assert.Nil(b, err)
	}

}
