package http

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	_ "bitbucket.org/VladimirCunichin/proxy/docs"

	"bitbucket.org/VladimirCunichin/proxy/internal/adapters/cache"
	"bitbucket.org/VladimirCunichin/proxy/internal/adapters/inmemory"
	"bitbucket.org/VladimirCunichin/proxy/internal/adapters/sql"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/config"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/usecases"
	"bitbucket.org/VladimirCunichin/proxy/internal/logger"
	"github.com/gorilla/mux"
	swag "github.com/swaggo/http-swagger"
)

type Server struct {
	cache          cache.LRUCache
	requestStorage *usecases.RequestStorage
	ctx            context.Context
}

func CustomClient() *http.Client {
	client := &http.Client{
		Transport: &http.Transport{
			Proxy: http.ProxyFromEnvironment,
			Dial: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
			}).Dial,
			TLSHandshakeTimeout: 10 * time.Second,
			DisableCompression:  true,
		}}

	return client
}

func InitServer(listenIP, listenPort string, cacheSize int, conf *config.Config) {
	server := &Server{
		cache: cache.NewLRU(cacheSize),
		requestStorage: func() *usecases.RequestStorage {
			if strings.EqualFold(conf.Storage, "sql") {
				return usecases.New(sql.NewStorage(conf))
			}
			return usecases.New(inmemory.NewStorage())
		}(),
		ctx: context.Background(),
	}
	router := mux.NewRouter()
	router.PathPrefix("/swagger/").Handler(swag.WrapHandler)

	router.HandleFunc("/", server.add).Methods("POST")
	router.HandleFunc("/", server.get).Methods("GET")
	router.HandleFunc("/getAll", server.getAll).Methods("GET")
	router.HandleFunc("/", server.delete).Methods("DELETE")

	srv := &http.Server{
		Addr:         listenIP + ":" + listenPort,
		Handler:      router,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		err := srv.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			logger.Fatal("Error while starting HTTP server", "error", err)
		}
	}()
	logger.Info(fmt.Sprintf("HTTP server started on host: %s, port: %s", listenIP, listenPort))
	<-done
	logger.Info("HTTP server stopped")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		logger.Fatal("Server shutdown error", "error", err)
	}
	logger.Info("server shut down properly")
}
