package http

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"bitbucket.org/VladimirCunichin/proxy/internal/adapters/cache"
	"bitbucket.org/VladimirCunichin/proxy/internal/adapters/inmemory"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/entities"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/usecases"
	"bitbucket.org/VladimirCunichin/proxy/internal/logger"
	"github.com/stretchr/testify/assert"
)

func initServer() *Server {
	logger.Init("debug", "log.log")
	return &Server{
		cache:          cache.NewLRU(3),
		requestStorage: usecases.New(inmemory.NewStorage()),
		ctx:            context.Background(),
	}
}

var server *Server = initServer()

func add(t *testing.T) entities.Response {
	body := strings.NewReader(`{"method":"GET","url":"https://jsonplaceholder.typicode.com/posts","headers":{"Authentication":["Basic\tbG9naW46cGFzc3dvcmQ="],"Content-Type":["application/json"]}}`)
	req := httptest.NewRequest(http.MethodGet, "/", body)
	w := httptest.NewRecorder()
	server.add(w, req)
	res := w.Result()
	data, err := ioutil.ReadAll(res.Body)
	assert.Nil(t, err)
	var response entities.Response
	err = json.Unmarshal(data, &response)
	assert.Nil(t, err)
	return response
}

func TestAdd(t *testing.T) {
	response := add(t)
	assert.Equal(t, response.Status, http.StatusOK, "status should be 200")
	assert.Equal(t, response.Length, int64(27520), "status should be 200")
}

func TestGetNotFound(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "/?id=b5bbee8d-631f-454f-8327-2d5ce4564054", nil)
	w := httptest.NewRecorder()
	server.get(w, req)
	res := w.Result()
	defer res.Body.Close()
	assert.Equal(t, http.StatusNotFound, res.StatusCode, "code should be 404")
}

func TestDeleteNotFound(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "/", strings.NewReader(`{"id":"b5bbee8d-631f-454f-8327-2d5ce4564054"}`))
	w := httptest.NewRecorder()
	server.delete(w, req)
	res := w.Result()
	defer res.Body.Close()
	assert.Equal(t, http.StatusNotFound, res.StatusCode, "code should be 404")
}
func TestGet(t *testing.T) {
	added := add(t)
	url := fmt.Sprintf(`/?id=%s`, added.Id.String())
	req := httptest.NewRequest(http.MethodGet, url, nil)
	w := httptest.NewRecorder()
	server.get(w, req)
	res := w.Result()
	defer res.Body.Close()
	assert.Equal(t, http.StatusOK, res.StatusCode, "code should be 200")
}

func TestDelete(t *testing.T) {
	added := add(t)
	url := fmt.Sprintf(`/?id=%s`, added.Id.String())
	req := httptest.NewRequest(http.MethodGet, url, nil)
	w := httptest.NewRecorder()
	server.get(w, req)
	res := w.Result()
	defer res.Body.Close()
	assert.Equal(t, http.StatusOK, res.StatusCode, "code should be 404")
}
