package http

import (
	"bytes"
	"context"
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"bitbucket.org/VladimirCunichin/proxy/internal/domain/entities"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/errors"
	"bitbucket.org/VladimirCunichin/proxy/internal/logger"
	uuid "github.com/satori/go.uuid"
)

func Post(url string, body interface{}, headers entities.Headers) (*http.Response, error) {
	jsonBytes, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}
	request, err := http.NewRequest(http.MethodPost, url, bytes.NewReader(jsonBytes))
	if err != nil {
		return nil, err
	}
	request.Header = map[string][]string(headers)
	client := CustomClient()
	return client.Do(request)
}

func SHA1(text string) string {
	algorithm := sha1.New()
	algorithm.Write([]byte(text))
	return hex.EncodeToString(algorithm.Sum(nil))
}

//@Summary SendRequest
//@Tags proxy
//@Description send a new proxy reqeust
//@ID send-request
//@Accept json
//@Produce json
//@Param input body entities.Request true "request info"
//@Success 200 {object} entities.Response "success"
//@Failure 400 {object} errors.SwagBadRequestError "Bad Request"
//@Failure 404 {object} errors.SwagNotFoundError "Not Found"
//@Failure 500 {object} errors.SwagInternalError "Internal Server Error"
//@Failure default {object} errors.SwagInternalError "Internal Server Error"
//@Router / [post]
func (s Server) add(w http.ResponseWriter, r *http.Request) {
	client := CustomClient()
	var req entities.Request
	reqBody, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(reqBody, &req)
	if err != nil {
		logger.Error("error during unmarshal of request", "Error", err)
	}
	cacheReq, err := json.Marshal(req)
	if err != nil {
		logger.Error("error marshalling req", "err", err)
	}
	cacheKey := SHA1(string(cacheReq))
	if cacheResp, err := s.cache.Get(cacheKey); err == nil {
		json.NewEncoder(w).Encode(cacheResp)
		return
	}

	switch req.Method {
	case "GET":
		request, err := http.NewRequest("GET", req.Url, nil)
		if err != nil {
			logger.Error("error while creating a http request", "error", err)
			return
		}
		for k, v := range req.RequestHeaders {
			for _, header := range v {
				request.Header.Add(k, header)
			}
		}
		resp, err := client.Do(request)
		if err != nil {
			logger.Error("error while doing the request", "err", err)
			return
		}
		if resp.ContentLength == -1 {
			b, _ := ioutil.ReadAll(resp.Body)
			resp.ContentLength = int64(len(b))
		}

		defer resp.Body.Close()

		response := entities.Response{
			Id:      uuid.NewV4(),
			Status:  resp.StatusCode,
			Headers: map[string][]string(resp.Header),
			Length:  resp.ContentLength,
		}
		err = s.requestStorage.SaveData(context.Background(), &req, &response)
		if err != nil {
			logger.Error("error during save data", "error", err)
		}
		s.cache.Set(cacheKey, &response)
		err = json.NewEncoder(w).Encode(response)
		if err != nil {
			logger.Error("error during json encode", "error", err)
		}
	case "POST":
		resp, err := Post(req.Url, req.Body, req.RequestHeaders)
		if err != nil {
			logger.Error("error while posting", "err", err)
			return
		}
		if resp.ContentLength == -1 {
			b, _ := ioutil.ReadAll(resp.Body)
			resp.ContentLength = int64(len(b))
		}
		defer resp.Body.Close()
		response := entities.Response{
			Id:      uuid.NewV4(),
			Status:  resp.StatusCode,
			Headers: map[string][]string(resp.Header),
			Length:  resp.ContentLength,
		}
		err = s.requestStorage.SaveData(context.Background(), &req, &response)
		s.cache.Set(cacheKey, &response)
		if err != nil {
			logger.Error("error during save data", "error", err)
		}
		err = json.NewEncoder(w).Encode(response)
		if err != nil {
			logger.Error("error during json encode", "error", err)
		}
	default:
		logger.Error("Unexpected command", "command", req.Method)
		return
	}
}

//@Summary GetAllRequests
//@Tags proxy
//@Description get all saved request data
//@ID getAll-request
//@Accept json
//@Produce json
//@Success 200 {array} []entities.SaveData "success"
//@Failure 400 {object} errors.SwagBadRequestError "Bad Request"
//@Failure 404 {object} errors.SwagNotFoundError "Not Found"
//@Failure 500 {object} errors.SwagInternalError "Internal Server Error"
//@Failure default {object} errors.SwagInternalError "Internal Server Error"
//@Router /getAll [get]
func (s Server) getAll(w http.ResponseWriter, r *http.Request) {
	requests, err := s.requestStorage.GetData(context.Background())
	if err != nil {
		logger.Error("error while getting data from storage", "error", err)
	}
	err = json.NewEncoder(w).Encode(requests)
	if err != nil {
		logger.Error("error during json encode", "error", err)
	}
}

//@Summary GetRequest
//@Tags proxy
//@Description get saved request data
//@ID get-request
//@Accept json
//@Produce json
//@Param id query string true "saved data id"
//@Success 200 {object} entities.SaveData "success"
//@Failure 400 {object} errors.SwagBadRequestError "Bad Request"
//@Failure 404 {object} errors.SwagNotFoundError "Not Found"
//@Failure 500 {object} errors.SwagInternalError "Internal Server Error"
//@Failure default {object} errors.SwagInternalError "Internal Server Error"
//@Router / [get]
func (s Server) get(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	queryId, present := query["id"]
	if !present {
		w.WriteHeader(http.StatusBadRequest)
		err := json.NewEncoder(w).Encode(errors.BadRequest.Error())
		if err != nil {
			logger.Error("error during json encode", "error", err)
		}
		return
	}
	id, err := uuid.FromString(queryId[0])
	if err != nil {
		logger.Error("error while converting string to uuid", "error", err)
		w.WriteHeader(http.StatusBadRequest)
		err := json.NewEncoder(w).Encode(errors.BadRequest.Error())
		if err != nil {
			logger.Error("error during json encode", "error", err)
		}
		return
	}
	data, err := s.requestStorage.GetDataByID(context.Background(), id)
	if err != nil {
		if err == errors.NotFound {
			logger.Error("error data not found in storage", "err", err)
			w.WriteHeader(http.StatusNotFound)
			return
		}
		logger.Error("error while getting data from storage", "err", err)
	}

	err = json.NewEncoder(w).Encode(data)
	if err != nil {
		logger.Error("error during json encode", "error", err)
	}
}

//@Summary DeleteRequest
//@Tags proxy
//@Description delete saved request reqeust
//@ID delete-request
//@Accept json
//@Produce json
//@Param input body entities.SwagUUID true "request info"
//@Success 200 {object} entities.SaveData "success"
//@Failure 400 {object} errors.SwagBadRequestError "Bad Request"
//@Failure 404 {object} errors.SwagNotFoundError "Not Found"
//@Failure 500 {object} errors.SwagInternalError "Internal Server Error"
//@Failure default {object} errors.SwagInternalError "Internal Server Error"
//@Router / [delete]
func (s Server) delete(w http.ResponseWriter, r *http.Request) {
	body := make(map[string]uuid.UUID)
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.Error("error reading req body", "err", err)
	}
	err = json.Unmarshal(reqBody, &body)
	if err != nil {
		logger.Error("error while unmarshaling id", "err", err)
	}
	id, ok := body["id"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	data, err := s.requestStorage.GetDataByID(context.Background(), id)
	if err != nil {
		if err == errors.NotFound {
			logger.Error("error data not found in storage", "err", err)
			w.WriteHeader(http.StatusNotFound)
			return
		}
		logger.Error("error while getting data from storage", "err", err)
	}
	cacheKey, err := json.Marshal(&data.Request)
	if err != nil {
		logger.Error("error while marshaling to json", "err", err)
	}
	s.cache.Remove(SHA1(string(cacheKey)))
	err = s.requestStorage.Delete(context.Background(), id)
	if err != nil {
		if err == errors.NotFound {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		logger.Error("error deleting data from storage", "err", err)
	}
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode("deleted successfully")
	if err != nil {
		logger.Error("error while writing", "error", err)
	}

}
