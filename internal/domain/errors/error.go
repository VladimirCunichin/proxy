package errors

import "errors"

var (
	InternalError     = errors.New("internal server error")
	NotFound          = errors.New("404 not found")
	BadRequest        = errors.New("400 bad request")
	DataAlreadyExists = errors.New("this Id already exists")
	ConnConfigError   = errors.New("failed to parse connection config")
	DBConnError       = errors.New("failed to connect to db")
)

type SwagInternalError struct {
	Error string `example:"Internal Error"`
}

type SwagBadRequestError struct {
	Error string `example:"Bad Request"`
}

type SwagNotFoundError struct {
	Error string `example:"Not Found"`
}
