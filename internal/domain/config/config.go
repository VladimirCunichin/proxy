package config

import (
	"os"
	"strconv"

	"github.com/spf13/viper"
)

type Config struct {
	LogFile    string `json:"LOG_FILE" mapstructure:"LOG_FILE"`
	LogLevel   string `json:"LOG_LEVEL" mapstructure:"LOG_LEVEL"`
	CacheSize  int    `json:"CACHE_SIZE" mapstructure:"CACHE_SIZE"`
	HttpIp     string `json:"HTTP_IP" mapstructure:"HTTP_IP"`
	HttpPort   int    `json:"HTTP_PORT" mapstructure:"HTTP_PORT"`
	DBUser     string `json:"DB_USER" mapstructure:"DB_USER"`
	DBPassword string `json:"DB_PASSWORD" mapstructure:"DB_PASSWORD"`
	DBHost     string `json:"DB_HOST" mapstructure:"DB_HOST"`
	DBPort     string `json:"DB_PORT" mapstructure:"DB_PORT"`
	DBDatabase string `json:"DB_DATABASE" mapstructure:"DB_DATABASE"`
	Storage    string `json:"STORAGE_TYPE" mapstructure:"STORAGE_TYPE"`
}

func LoadConfig(path string) (config *Config, err error) {
	viper.SetConfigFile(path)
	viper.AutomaticEnv()
	err = viper.ReadInConfig()
	if err != nil {
		return
	}
	err = viper.Unmarshal(&config)
	os.Setenv("LOG_FILE", config.LogFile)
	os.Setenv("LOG_LEVEL", config.LogLevel)
	os.Setenv("CACHE_SIZE", strconv.Itoa(config.CacheSize))
	os.Setenv("HTTP_IP", config.HttpIp)
	os.Setenv("HTTP_PORT", strconv.Itoa(config.HttpPort))
	os.Setenv("DB_USER", config.DBUser)
	os.Setenv("DB_PASSWORD", config.DBPassword)
	os.Setenv("DB_HOST", config.DBHost)
	os.Setenv("DB_PORT", config.DBPort)
	os.Setenv("DB_DATABASE", config.DBDatabase)
	os.Setenv("STORAGE_TYPE", config.Storage)
	return
}
