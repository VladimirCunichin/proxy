package usecases

import (
	"context"

	"bitbucket.org/VladimirCunichin/proxy/internal/domain/entities"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/errors"
	"bitbucket.org/VladimirCunichin/proxy/internal/domain/interfaces"
	uuid "github.com/satori/go.uuid"
)

type RequestStorage struct {
	Storage interfaces.Storage
}

func New(storage interfaces.Storage) *RequestStorage {
	return &RequestStorage{Storage: storage}
}

func (sr *RequestStorage) SaveData(ctx context.Context, request *entities.Request, response *entities.Response) error {
	saveData := entities.SaveData{
		Id:      response.Id,
		Request: request,
		Response: &entities.SaveResponse{
			Status:  response.Status,
			Headers: response.Headers,
			Length:  response.Length,
		},
	}
	err := sr.Storage.SaveData(context.Background(), saveData)
	return err
}

func (sr *RequestStorage) GetDataByID(ctx context.Context, id uuid.UUID) (entities.SaveData, error) {
	data, err := sr.Storage.GetDataByID(context.Background(), id)
	if data == (entities.SaveData{}) {
		return data, errors.NotFound
	}
	return data, err
}

func (sr *RequestStorage) GetData(ctx context.Context) ([]entities.SaveData, error) {
	data, err := sr.Storage.GetData(context.Background())
	if len(data) == 0 {
		return data, errors.NotFound
	}
	return data, err
}

func (sr *RequestStorage) Delete(ctx context.Context, id uuid.UUID) error {
	return sr.Storage.Delete(context.Background(), id)
}

func (sr *RequestStorage) GetResponse(ctx context.Context, id uuid.UUID) (entities.Response, error) {
	savedData, err := sr.Storage.GetDataByID(context.Background(), id)
	if err != nil {
		return entities.Response{}, err
	}
	return entities.Response{
		Id:      savedData.Id,
		Status:  savedData.Response.Status,
		Headers: savedData.Response.Headers,
		Length:  savedData.Response.Length,
	}, nil

}
