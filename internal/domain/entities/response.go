package entities

import uuid "github.com/satori/go.uuid"

type Response struct {
	//Response id
	Id uuid.UUID `json:"id" db:"id" example:"123e4567-e89b-12d3-a456-426614174000"`
	//Response status
	Status int `json:"status" db:"status" example:"200"`
	//Response headers
	Headers Headers `json:"headers" db:"headers" sql:"type:jsonb"`
	//Response length
	Length int64 `json:"length" db:"length" example:"123"`
}
