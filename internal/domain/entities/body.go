package entities

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
)

type Body map[string]interface{}

func (b Body) Value() (driver.Value, error) {
	return json.Marshal(b)
}

func (b *Body) Scan(v interface{}) error {
	if v == nil {
		return nil
	}
	switch data := v.(type) {
	case string:
		return json.Unmarshal([]byte(data), &b)
	case []byte:
		return json.Unmarshal(data, &b)
	default:
		return fmt.Errorf("cannot scan type %t into Map", v)
	}
}
