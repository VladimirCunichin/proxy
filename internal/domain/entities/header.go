package entities

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
)

type Headers map[string][]string

func (h Headers) Value() (driver.Value, error) {
	return json.Marshal(h)
}

func (h *Headers) Scan(v interface{}) error {
	if v == nil {
		return nil
	}
	switch data := v.(type) {
	case string:
		return json.Unmarshal([]byte(data), &h)
	case []byte:
		return json.Unmarshal(data, &h)
	default:
		return fmt.Errorf("cannot scan type %t into Map", v)
	}
}
