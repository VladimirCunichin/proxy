package entities

import (
	uuid "github.com/satori/go.uuid"
)

type SaveResponse struct {
	Status  int     `json:"status" db:"status"`
	Headers Headers `json:"headers" db:"headers" sql:"type:jsonb"`
	Length  int64   `json:"length" db:"length"`
}

type SaveData struct {
	Id       uuid.UUID     `json:"id" db:"id" example:"123e4567-e89b-12d3-a456-426614174000"`
	Request  *Request      `json:"request" db:"request" gorm:"embedded"`
	Response *SaveResponse `json:"response" db:"response" gorm:"embedded"`
}
