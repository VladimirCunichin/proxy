package entities

import uuid "github.com/satori/go.uuid"

type Request struct {
	//Requests method
	Method string `json:"method" db:"method" example:"GET"`
	//Url of request
	Url string `json:"url" db:"url" example:"www.google.com"`
	//Requests Headers
	RequestHeaders Headers `json:"headers" db:"request_headers" sql:"type:jsonb" example:"Headers:{
			"Authentication":["Basic\tbG9naW46cGFzc3dvcmQ="],
			"Content-Type":["application/json"]
		}"`
	//Requests Body
	Body Body `json:"body" db:"body" sql:"type:jsonb" example""`
}

type SwagUUID struct {
	ID uuid.UUID `json:"id" example:"b876f2c7-4af7-4839-9c01-81dd5ec4e634"`
}
