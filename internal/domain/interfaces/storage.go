package interfaces

import (
	"context"

	"bitbucket.org/VladimirCunichin/proxy/internal/domain/entities"
	uuid "github.com/satori/go.uuid"
)

type Storage interface {
	SaveData(ctx context.Context, data entities.SaveData) error
	GetDataByID(ctx context.Context, id uuid.UUID) (entities.SaveData, error)
	GetData(ctx context.Context) ([]entities.SaveData, error)
	Delete(ctx context.Context, id uuid.UUID) error
}

type Cache interface {
	Get(key string) *entities.Response
	set(key string, value *entities.Response)
}
