run:
	go run cmd/main.go
test:
	go test ./internal/http/... ./internal/adapters/inmemory/... ./internal/adapters/sql/...
bench:
	go test -bench=BenchmarkAdd ./internal/http/...
dcbuild:
	docker-compose build
dcup:
	docker-compose up
clearmem:
	docker system prune -a
swag-doc:
	swag init -g cmd/main.go
swag-gen:
	swagger generate server --default-scheme=proxy --exclude-main --flag-strategy=flag -f api/swagger.yml -t internal/pkg/proxy/