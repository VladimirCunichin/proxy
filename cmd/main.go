package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"bitbucket.org/VladimirCunichin/proxy/internal/domain/config"
	"bitbucket.org/VladimirCunichin/proxy/internal/logger"
	"bitbucket.org/VladimirCunichin/proxy/internal/pkg/proxy/restapi"
	"bitbucket.org/VladimirCunichin/proxy/internal/pkg/proxy/restapi/operations"
	"github.com/go-openapi/loads"
)

//go:generate swagger generate spec
//@title proxy
//@version 1.0.0
//@description API server for proxy application

//@host 127.0.0.1:8080
//@BasePath /
func main() {
	config, err := config.LoadConfig("config/config.env")
	if err != nil {
		log.Fatal("Can't read configuration file", "error", err)
	}
	logger.Init(config.LogLevel, config.LogFile)
	logger.Info("started")

	swaggerSpec, err := loads.Embedded(restapi.SwaggerJSON, restapi.FlatSwaggerJSON)
	if err != nil {
		log.Fatalln(err)
	}
	var server *restapi.Server // make sure init is called

	flag.Usage = func() {
		fmt.Fprint(os.Stderr, "Usage:\n")
		fmt.Fprint(os.Stderr, "  proxy-application-server [OPTIONS]\n\n")

		title := "Proxy application"
		fmt.Fprint(os.Stderr, title+"\n\n")
		desc := "A proxy application that receives information about request it has to make and sends back response data"
		if desc != "" {
			fmt.Fprintf(os.Stderr, desc+"\n\n")
		}
		flag.CommandLine.SetOutput(os.Stderr)
		flag.PrintDefaults()
	}
	// parse the CLI flags
	flag.Parse()
	api := operations.NewProxyApplicationAPI(swaggerSpec)
	// get server with flag values filled out
	server = restapi.NewServer(api)
	defer server.Shutdown()
	server.Host = config.HttpIp
	server.Port = config.HttpPort
	server.ConfigureAPI()
	if err := server.Serve(); err != nil {
		log.Fatalln(err)
	}
}
